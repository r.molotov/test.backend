﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Notes.Application.Notes.Queries.GetNoteDetails;
using Notes.Application.Notes.Queries.GetNoteList;
using Notes.Tests.Common;
using NotesPersistence;
using Shouldly;
using Xunit;

namespace Notes.Tests.Queries
{
    [Collection("QueryCollection")]
    public class GetNoteDetailsQueryHandlerTests
    {
        private readonly NotesDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetNoteDetailsQueryHandlerTests(QueryTestFixture fixture) =>
            (_dbContext, _mapper) = (fixture.Context, fixture.Mapper);

        [Fact]
        public async Task GetNoteDetailsQueryHandler_Success()
        {
            //Arrange
            var handler = new GetNoteDetailsQueryHandler(_dbContext, _mapper);

            //Act
            var result = await handler.Handle(
                new GetNoteDetailsQuery
                {
                    UserId = NotesContextFactory.UserAId,
                    Id = Guid.Parse("54AF4FC3-7FEC-440F-840B-C0D580271432")
                },
                CancellationToken.None
            );

            //Assert
            result.ShouldBeOfType<NoteDetailsVm>();
            result.Title.ShouldBe("Title1");
            result.CreationDate.ShouldBe(DateTime.Today);
        }
    }
}
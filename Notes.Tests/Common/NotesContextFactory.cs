﻿using System;
using Microsoft.EntityFrameworkCore;
using NotesPersistence;
using Microsoft.EntityFrameworkCore.InMemory;
using Notes.Domain;

namespace Notes.Tests.Common
{
    public class NotesContextFactory
    {
        public static Guid UserAId = Guid.NewGuid();
        public static Guid UserBId = Guid.NewGuid();

        public static Guid NoteIdForUpdate = Guid.NewGuid();
        public static Guid NoteIdForDelete = Guid.NewGuid();

        public static NotesDbContext Create()
        {
            var options = new DbContextOptionsBuilder<NotesDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            var context = new NotesDbContext(options);
            context.Database.EnsureCreated();

            context.Notes.AddRange(
                new Note
                {
                    CreationDate = DateTime.Today,
                    EditDate = null,
                    Title = "Title1",
                    Details = "Details1",
                    Id = Guid.Parse("54AF4FC3-7FEC-440F-840B-C0D580271432"),
                    UserId = UserAId
                },
                new Note
                {
                    CreationDate = DateTime.Today,
                    EditDate = null,
                    Title = "Title2",
                    Details = "Details2",
                    Id = Guid.Parse("024D271E-E9D7-4625-A9EA-500C982217C5"),
                    UserId = UserBId
                },
                new Note
                {
                    CreationDate = DateTime.Today,
                    EditDate = null,
                    Title = "Title3",
                    Details = "Details3",
                    Id = NoteIdForDelete,
                    UserId = UserAId
                },
                new Note
                {
                    CreationDate = DateTime.Today,
                    EditDate = null,
                    Title = "Title4",
                    Details = "Details4",
                    Id = NoteIdForUpdate,
                    UserId = UserAId
                }
            );
            context.SaveChanges();
            return context;
        }

        public static void Destroy(NotesDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Dispose();
        }
    }
}